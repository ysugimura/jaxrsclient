package com.cm55.jaxrsGen.rs;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

import javax.ws.rs.*;

import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.rs.RsParameter.*;
import com.cm55.jaxrsGen.util.*;

/**
 * ウェブメソッド

 * @author ysugimura
 */
public class RsMethod {

  /** メソッドの所属するウェブリソース */
  public final RsResource webResource;
  
  /** Javaメソッド名称 */
  public final String methodName;
  
  /** コメント */
  public final Optional<String>comment;
  
  /** 
   * メソッドのPathアノテーション。
   * 存在しないものもある。その場合は、クラスの@Pathが使用される。
   */
  public final Optional<String> methodPath;
  
  /** 必須。リクエストメソッド名称。GET, PUT, DELETE */
  public final HttpMethod httpMethod;
  
  /** 返すMimeType */
  public final Optional<RsProduces>produces;
  
  /** 受け取るMimeType */
  public final Optional<RsConsumes>consumes;
    
  /** メソッドの全パラメータ */
  private final List<RsParameter>webParams = new ArrayList<>();
  public Stream<RsParameter>webParams() { return webParams.stream(); }
  
  /** PathParamのみのパラメータ */
  private final List<RsPathParam>webPathParams = new ArrayList<>();
  public Stream<RsPathParam>webPathParams() { return webPathParams.stream(); }

  /** QueryParamのみのパラメータ */
  private final List<RsQueryParam>webQueryParams = new ArrayList<>();
  public Stream<RsQueryParam>webQueryParams() { return webQueryParams.stream(); }

  /** PathParamでもQueryParamでもないパラメータ。0か1個 */
  public final Optional<RsNoParam>webNoParam;
  
  /** メソッドの返り値 */
  public final JType returnType;

  /** エラー表示等に使用する、「クラス名#メソッド名」文字列 */
  public final String methodPosition;
  
  /** Javaメソッドを指定する */
  @SuppressWarnings("unchecked")
  public RsMethod(JTypeGenerator jgen, RsResource webResource, Method method) {
    
    this.webResource = webResource;    
    this.methodName = method.getName();
    
    methodPosition = webResource.fullClassName + "#" + methodName;
    
    // このメソッドのアノテーションを取得する
    JavaAnnoSet set = new JavaAnnoSet(method.getAnnotations());
    comment = set.getComment();
    
    // Pathアノテーションを取得。存在しない場合もある
    methodPath = set.removeOptional(Path.class).map(p->p.value());
    
    // GET, PUT, DELETE, POST, HEADの取得
    List<? extends Annotation>methodAnnos = set.remove(new Class[] {
      GET.class,
      PUT.class,
      POST.class,
      HEAD.class,
      DELETE.class
    });
    switch (methodAnnos.size()) {
    case 0:
      throw new AnalyException(methodPosition +"：GET,POST等メソッド指定無し");
    case 1:
      httpMethod = HttpMethod.get(methodAnnos.get(0));
      break;
    default:
      throw new AnalyException(methodPosition + "：GET,POST等メソッド指定複数");      
    }
    
    // 
    produces = set.removeOptional(Produces.class, RsProduces::create);
    consumes = set.removeOptional(Consumes.class, RsConsumes::create);
    
    // このメソッドのフィールドを取得する 
    RsNoParam[]webNoParams = new RsNoParam[1];
    Arrays.stream(method.getParameters()).forEach(p-> {
      RsParameter wp = RsParameter.create(jgen, p);
      webParams.add(wp);
      
      if (wp instanceof RsPathParam) webPathParams.add((RsPathParam)wp);
      else if (wp instanceof RsQueryParam) webQueryParams.add((RsQueryParam)wp);
      else if (wp instanceof RsNoParam) {
        if (webNoParams[0] != null) {
          throw new AnalyException(methodPosition + ":アノテーションの無い複数のパラメータ");
        }
        webNoParams[0] = (RsNoParam)wp;
      }
      else throw new AnalyException("不明なエラー");
    });
    webNoParam = Optional.ofNullable(webNoParams[0]);

    // リクエストメソッドによるパラメータチェック
    if (webNoParam.isPresent()) {
      switch (httpMethod) {
      case GET: case DELETE:
        throw new AnalyException(methodPosition + "：GET/DELETEメソッドのパラメータが不正");
      default: break;
      }
    } else {
      switch (httpMethod) {
      case POST: case PUT:
        throw new AnalyException(methodPosition + "：POST/PUTメソッドのパラメータが不正");
      default: break;
      }
    }
    
    // 返り値型
    returnType = jgen.generateType(method.getGenericReturnType());
    
    // チェック
    pathParamCheck();
  }

  /** メソッドの返り値型の文字列を取得する */
  public String getReturnTypeString(JTypeStringizer stringizer) {
    return stringizer.stringize(returnType);
  }

  static Pattern PATH_ELEMENT_PATTERN = Pattern.compile("\\{([^\\}]*)\\}");
  
  /**
   * メソッドの@Pathとパラメータの@PathParamのチェック
   */
  private void pathParamCheck() {
    // メソッドの@Pathが存在しないとき、@PathParamは存在しない
    if (!methodPath.isPresent()) {
      if (webPathParams.size() >0) throw new AnalyException("@Pathが無いのに@PathParamが存在している");
      return;
    }

    Set<String>elements = pathElements();
    this.webPathParams().forEach(p->elements.remove(p.pathParam));
    if (elements.size() > 0) {
      throw new AnalyException(methodPosition + ":@PathParamが無い。" +
          elements.stream().collect(Collectors.joining(",")));
    }
  }

  /**
   * @Pathに含まれる要素を取得する
   * @return 要素集合
   */
  private Set<String>pathElements() {
    Set<String>set = new HashSet<>();
    Matcher m = PATH_ELEMENT_PATTERN.matcher(methodPath.get());
    while (m.find()) {
      set.add(m.group(1));
    }    
    return set;
  }

}
