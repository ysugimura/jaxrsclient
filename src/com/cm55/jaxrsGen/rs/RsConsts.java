package com.cm55.jaxrsGen.rs;

public class RsConsts {

  /**
   * Responseクラスは特別な処理が必要
   */
  public static final String RESPONSE = "javax.ws.rs.core.Response";

}
