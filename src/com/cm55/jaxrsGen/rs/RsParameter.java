package com.cm55.jaxrsGen.rs;


import java.util.*;

import javax.ws.rs.*;

import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.util.*;

/**
 * メソッドパラメータ
 * <p>
 * 注意：Javaコンパイラがデバッグ情報を含めている場合はもともとのパラメータ名を
 * 取得できるが、そうでない場合にはパラメータ名はarg0, arg1...などとなる。
 * </p>
 * @author ysugimura
 */
public class RsParameter {


  /** パラメータ名称 */
  public String name;
  
  /** Javaのパラメータ定義 */
  public final JType type;
  
  /** コメント */
  public final Optional<String>comment;
  
  /**
   * 指定されたJavaのパラメータについて作成する
   * @param jgen Jsonタイプジェネレータ
   * @param parameter Javaのパラメータ
   * @return {@link RsParameter}
   */
  public static RsParameter create(JTypeGenerator jgen, java.lang.reflect.Parameter parameter) {
    
    // このパラメータのアノテーションすべてを取得する
    JavaAnnoSet set = new JavaAnnoSet(parameter.getAnnotations());
    
    // PathParamアノテーションがある場合
    Optional<PathParam>pathParam = set.removeOptional(PathParam.class);
    if (pathParam.isPresent()) return new RsPathParam(jgen, parameter, pathParam.get(), set);
    
    // QueryParamアノテーションがある場合
    Optional<QueryParam>queryParam = set.removeOptional(QueryParam.class);
    if (queryParam.isPresent()) return new RsQueryParam(jgen, parameter, queryParam.get(), set);

    // アノテーション無しの場合
    return new RsNoParam(jgen, parameter, set);

  }
  
  /** 一つのメソッドパラメータについて作成する */
  private RsParameter(JTypeGenerator jgen, java.lang.reflect.Parameter parameter, JavaAnnoSet set) {
    this.comment = set.getComment();
    this.name = parameter.getName();
    this.type = jgen.generateType(parameter.getParameterizedType());

  }
 
  /** このパラメータのタイプ文字列を取得する */
  public String getParamTypeString(JTypeStringizer stringizer) {
    return stringizer.stringize(type);
  }

  
  /** 
   * PathParamパラメータ
   * @author ysugimura
   */
  public static class RsPathParam extends RsParameter {
    public final String pathParam;
    RsPathParam(JTypeGenerator jgen, java.lang.reflect.Parameter parameter, PathParam pp, JavaAnnoSet set) {
      super(jgen, parameter, set);
      pathParam = pp.value();
    }
    @Override
    public String toString() {
      return "@PathParam(\"" + pathParam + "\") " + super.toString();
    }
  }

  /**
   * QueryParamパラメータ
   * @author ysugimura
   */
  public static class RsQueryParam extends RsParameter {
    public final String queryParam;
    
    RsQueryParam(JTypeGenerator jgen, java.lang.reflect.Parameter parameter, QueryParam qp, JavaAnnoSet set) {
      super(jgen, parameter, set);
      queryParam = qp.value();
    }
    @Override
    public String toString() {
      return "@QueryParam(\"" + queryParam + "\") " + super.toString();
    }
  }

  /**
   * アノテーション無しパラメータ
   * @author ysugimura
   */
  public static class RsNoParam extends RsParameter {
    RsNoParam(JTypeGenerator jgen, java.lang.reflect.Parameter parameter, JavaAnnoSet set) {
      super(jgen, parameter, set);
    }
  }
}
