package com.cm55.jaxrsGen.rs;

import java.lang.annotation.*;
import java.util.*;

public enum HttpMethod {
  GET("get"),
  PUT("put"),
  POST("post"),
  HEAD("head"),
  DELETE("delete");  
  
  public final String lower;
  private HttpMethod(String lower) {
    this.lower = lower;
  }
  
  public static HttpMethod get(Annotation ann) {
    String name = ann.annotationType().getSimpleName();
    return Arrays.stream(HttpMethod.values())
        .filter(rm->rm.toString().equals(name))
        .findAny().get();
  }
}
