package com.cm55.jaxrsGen.rs;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

import javax.ws.rs.*;

import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.util.*;

/**
 * ウェブリソース
 * <p>
 * JAX-RSでアノテーションされたJavaのクラスは「ウェブリソース」と呼ばれている。
 * 例えば以下のクラスが「ウェブリソース」となる。
 * </p>
 * <pre>
 * &#064;Path("thing")
 * public class ThingResource {
 * }
 * </pre>
 * <p>
 * 「ウェブリソース」には必ずPathアノテーションが付加されている。
 * </p>
 * @author ysugimura
 */
public class RsResource {

  /** 
   * ウェブリソース出力クラス名。
   * 名前競合時は変更されることがある */
  public String outputClassName;
  
  /** 元のフルクラス名 */
  public final String fullClassName;
  
  /** ウェブリソースに必須のパスアノテーション */
  public final String resourcePath;
  
  /** コメント */
  public final Optional<String>comment;

  /** ウェブリソース中のウェブメソッド */
  private  List<RsMethod>rsMethods = new ArrayList<>();
  public Stream<RsMethod>webMethods() { return rsMethods.stream(); }
    
  /** Javaクラスを指定する */
  public RsResource(JTypeGenerator jgen, Class<?>javaClass) {
    
    // クラスオブジェクトを保存
    this.outputClassName = javaClass.getSimpleName();
    this.fullClassName = javaClass.getName();
    
    // このJavaクラスの全アノテーションを取得する
    JavaAnnoSet set = new JavaAnnoSet(javaClass.getAnnotations());
    comment = set.getComment();
    
    // 必須の@Pathアノテーションを取得する
    resourcePath = set.remove(Path.class).value();
   
    // メソッドについて処理。ここにはPathの指定の無いものもあるので注意
    Arrays.stream(javaClass.getDeclaredMethods())
      .filter(this::isRsMethod)
      .forEach(m->rsMethods.add(new RsMethod(jgen, this, m)));

    // メソッドのチェック
    checkMethods();
  }
  
  /**
   * JAX-RSのメソッドか調べる
   * @param method 対象メソッド
   * @return true:JAX-RSの処理メソッド
   */
  private boolean isRsMethod(Method method) {
    String name = method.getName();
    
    // lambda$0等のメソッドが自動生成される
    if (name.indexOf('$') >= 0) return false;

    // javax.ws.rsアノテーションが一つでもついているものをJAX-RSメソッドとする
    JavaAnnoSet set = new JavaAnnoSet(method.getAnnotations());
    return set.names().filter(n->n.startsWith("javax.ws.rs.")).findAny().isPresent();
  }

  /**
   * メソッドのチェック
   * <p>
   * パス（無しも含む）が同じ、かつリクエストメソッドが同じJavaメソッドがあってはいけない。
   * </p>
   */
  private void checkMethods() {
    
    class Pair {
      final Optional<String>methodPath;
      final HttpMethod requestMethod;
      final RsMethod rsMethod;
      Pair(RsMethod rsMethod) {
        this.methodPath = rsMethod.methodPath;
        this.requestMethod = rsMethod.httpMethod;
        this.rsMethod = rsMethod;
      }
      @Override
      public int hashCode() {
        return methodPath.hashCode() + requestMethod.hashCode() * 7;
      }
      @Override
      public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;
        Pair that = (Pair)o;
        return this.methodPath.equals(that.methodPath) && 
            this.requestMethod.equals(that.requestMethod); 
      }
    }
    
    Set<Pair>pairSet = new HashSet<>();
    
    rsMethods.stream().forEach(rsMethod-> {
      Pair pair = new Pair(rsMethod);
      if (pairSet.contains(pair)) {
        if (!pair.methodPath.isPresent()) {
          throw new AnalyException(fullClassName + "：@Path無しで同じリクエストのメソッドが複数ある");
        } else {
          throw new AnalyException(fullClassName + "：@Path(\"" + 
              pair.methodPath.get() + "\")で同じリクエストのメソッドが複数ある");
          
        }
      }
      pairSet.add(pair);
    });

  }
}
