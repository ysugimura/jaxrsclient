package com.cm55.jaxrsGen.rs;

import java.util.*;
import java.util.stream.*;

import javax.ws.rs.*;

public class RsProduces {

  public String[]value;
  
  private RsProduces(String[]value) {
    this.value = value;
  }
  
  @Override
  public String toString() {
    String r = Arrays.stream(value).map(s->"\"" + s + "\"")
        .collect(Collectors.joining(","));
    if (value.length > 1) r = "{" + r + "}";
    return "@Produces(" + r + ")";
  }
  
  public static RsProduces create(Produces a) {
    return new RsProduces(a.value());
  }
}
