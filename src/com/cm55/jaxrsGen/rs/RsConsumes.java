package com.cm55.jaxrsGen.rs;

import java.util.*;
import java.util.stream.*;

import javax.ws.rs.*;

public class RsConsumes {

  public final String[]value;
  
  public RsConsumes(String[]value) {
    this.value = value;
  }
  
  @Override
  public String toString() {
    String r = Arrays.stream(value).map(s->"\"" + s + "\"")
        .collect(Collectors.joining(","));
    if (value.length > 1) r = "{" + r + "}";
    return "@Consumes(" + r + ")";
  }

  public static RsConsumes create(Consumes a) {
    return new RsConsumes(a.value());
  }
  
}
