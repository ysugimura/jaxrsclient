package com.cm55.jaxrsGen;

import java.util.*;
import java.util.stream.*;

import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.rs.*;

public class ClassCollection {

  
  // 全ウェブリソースのリスト
  final List<RsResource>webResources;

  // 全データのリスト
  final List<JClass>dataClasses;  

  public ClassCollection(List<RsResource>webResources, List<JClass>dataClasses) {
    this.webResources = webResources;
    this.dataClasses = dataClasses;
  }

  
  /** 全{@link RsResource}クラスを取得する */
  public Stream<RsResource>webResources() {
    return webResources.stream();
  }

  /** 全{@link JClass}クラスを取得する */
  public Stream<JClass>dataClasses() {
    return dataClasses.stream();
  }  
 
}
