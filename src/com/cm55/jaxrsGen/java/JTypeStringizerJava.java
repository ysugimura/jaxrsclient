package com.cm55.jaxrsGen.java;

import java.util.*;

import com.cm55.jaxrsGen.jtype.*;

public class JTypeStringizerJava implements JTypeStringizer {

  public static final JTypeStringizerJava INSTANCE = new JTypeStringizerJava();
  
  static final Map<JPrimitivable, String>PRIMITIVABLE_MAP = new HashMap<JPrimitivable, String>() {{
    put(JInt.PRIMITIVE, "int");
    put(JInt.OBJECT, "Integer");
    put(JLong.PRIMITIVE, "long");
    put(JLong.OBJECT, "Long");
    put(JFloat.PRIMITIVE, "float");
    put(JFloat.OBJECT, "Float");
    put(JDouble.PRIMITIVE, "double");
    put(JDouble.OBJECT, "Double");
    put(JBoolean.PRIMITIVE, "boolean");
    put(JBoolean.OBJECT, "Boolean");
  }};
  
  @Override
  public String stringize(JType type) {
    if (type instanceof JPrimitivable) {
      return PRIMITIVABLE_MAP.get((JPrimitivable)type);
    }
    if (type instanceof JClass) {
      return ((JClass)type).outputClassName;
    }
    if (type instanceof JString) {
      return "String";
    }
    if (type instanceof JArray) {
      JArray array = (JArray)type;
      return "java.util.List<" + stringize(array.element) + ">";
    }
    if (type instanceof JMap) {
      JMap map = (JMap)type;
      return "java.util.Map<" + stringize(map.key) + "," + stringize(map.value)+ ">";  
    }
    throw new RuntimeException();
  }

}
