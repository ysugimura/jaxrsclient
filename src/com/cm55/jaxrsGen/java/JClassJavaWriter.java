package com.cm55.jaxrsGen.java;

import java.util.stream.*;

import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.util.*;

public class JClassJavaWriter {

  public String generate(JClass jclass, String pkgName) {
    
    Stringize s = new Stringize(JavaCommenter.INSTANCE);    
    s.println("package " + pkgName + ";");
    s.println("");
    
    s.comment(jclass.comment);
    s.println("public class %s {", jclass.outputClassName);
    s.indent();
    try {
      jclass.fields.stream().forEach(d-> {
        s.println("");
        s.comment(d.comment);
        s.println("public %s %s;", 
          JTypeStringizerJava.INSTANCE.stringize(d.type), 
          d.name
        );
      });
      
      s.println("@Override public String toString() {");
      s.println("  return \"\" + " +
          jclass.fields.stream().map(f->f.name).collect(Collectors.joining(" + \",\" + ")) + ";");
      s.println("}");
      
    } finally {
      s.unindent();
    }
    s.append("}\n");
    
    return s.toString();
  }

}
