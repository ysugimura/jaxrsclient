package com.cm55.jaxrsGen.java;

import com.cm55.jaxrsGen.util.*;

public class JavaCommenter extends Commenter {

  public static final JavaCommenter INSTANCE = new JavaCommenter();
  
  private JavaCommenter() {
    super("/* ", " * ", " */");
  }

}
