package com.cm55.jaxrsGen.jtype;

/**
 * {@link JType}を文字列化する
 * @author ysugimura
 */
public interface JTypeStringizer {
  public String stringize(JType type);
}
