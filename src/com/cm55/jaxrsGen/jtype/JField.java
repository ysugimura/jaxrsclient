package com.cm55.jaxrsGen.jtype;

import java.util.*;

public class JField {

  public final String name;
  public final JType type;
  public final Optional<String>comment;
  
  public JField(String name, JType type, Optional<String>comment) {
    this.name = name;
    this.type = type;
    this.comment = comment;
  }
  
  @Override
  public String toString() {
    return type + " " + name;
  }

}
