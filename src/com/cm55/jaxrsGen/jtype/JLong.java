package com.cm55.jaxrsGen.jtype;

public class JLong extends JPrimitivable {
  
  public static final JLong PRIMITIVE = new JLong(true);
  public static final JLong OBJECT = new JLong(false);
  
  private JLong(boolean primitive) {
    super(primitive);
  }
  
  @Override
  public String toString() {
    return "long";
  }
}
