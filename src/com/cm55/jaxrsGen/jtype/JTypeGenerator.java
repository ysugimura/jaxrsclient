package com.cm55.jaxrsGen.jtype;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

import com.cm55.jaxrsGen.util.*;

/**
 * jtypeジェネレータ
 * @author ysugimura
 */
public class JTypeGenerator {
  
  /** クラスマップ */
  public Map<Class<?>, JClass> classMap = new HashMap<>();
  
  public Stream<JClass>jclasses() {
    return classMap.values().stream();
  }
  
  /** クラスを作成する */
  public JClass generateClass(Class<?>clazz) {
    JClass jclass = classMap.get(clazz);
    if (jclass != null) return jclass;
    
    // Object以外のスーパークラスは認めない
    Class<?> superClass = clazz.getSuperclass();
    if (superClass != null && !superClass.equals(Object.class)) {
      throw new AnalyException("Not supported:" + clazz.getName() + 
          " extends " + clazz.getSuperclass().getName());
    }
    
    jclass = new JClass(
      clazz.getName(),
      clazz.getSimpleName(),
      getFields(clazz),
      getComment(clazz.getAnnotations())
    );
    classMap.put(clazz, jclass);
    return jclass;
  }

  /** 
   * 指定クラスのフィールドから{@link JField}リストを作成する
   * @param clazz
   * @return
   */
  private List<JField> getFields(Class<?>clazz) {
    return Arrays.stream(clazz.getDeclaredFields())
      .filter(f-> {
        int modifier = f.getModifiers();
        if ((modifier & Modifier.STATIC) != 0) return false;
        return true;
      })
      .map(f->generateField(f))
      .collect(Collectors.toList());    
  }

  Optional<String>getComment(Annotation[]annos) {
    return new JavaAnnoSet(annos).getComment();
  }
  
  /** フィールドを作成する */
  JField generateField(Field field) {   
    return new JField(
      field.getName(),
      generateType(field.getGenericType()),
      getComment(field.getAnnotations())
    );
  }

  /** パラメータを作成する */
  JParameter generateParameter(Parameter p) {
    return new JParameter(p.getName(), generateType(p.getParameterizedType()));
  }
  
  /* ======================================================================= */
  
  /** 型を作成する */
  public JType generateType(Type type) {    
    
    // TypeがClass
    if (type instanceof Class) {
      Class<?>clazz = (Class<?>)type;
      if (clazz.isArray()) return arrayType(clazz);      
      return simpleClass((Class<?>)type);
    }

    // WildcardType
    if (type instanceof WildcardType) {
      throw new AnalyException("Wildcard not supported");
    }

    // GenericArrayType
    if (type instanceof GenericArrayType) {
      return genericArrayType((GenericArrayType)type);
    }
    
    // ParameterizedType
    return parameterizedType((ParameterizedType)type);
  }

  /** 
   * パラメータ付型
   * 「Map<String, Integer>」など
   * @param type
   * @return
   */
  JType parameterizedType(ParameterizedType type) {  
    
    // 内部クラスの場合の所有クラス
    /*
    Type ownerType = type.getOwnerType();        
    if (ownerType != null) getType(ownerType);  
    */
    
    // パラメータタイプ
    Type[]paramTypes = type.getActualTypeArguments();
    
    // 生タイプは配列かマップのいずれか
    Type rawType = type.getRawType();
    if (isMap(rawType)) {
      if (paramTypes.length != 2) throw new AnalyException("Map parameter not 2");
      return new JMap(generateType(paramTypes[0]), generateType(paramTypes[1]));
    }
    if (isArray(rawType)) {
      if (paramTypes.length != 1) throw new AnalyException("Array paramer not 1");
      return new JArray(generateType(paramTypes[0]));
    }
        
    throw new AnalyException("parametrizedType not supported:" + type);
  }
  
  /** このタイプがマップであるか */
  private boolean isMap(Type rawType) {
    if (!(rawType instanceof Class)) throw new AnalyException("Type is not a class");
    Class<?>clazz = (Class<?>)rawType;
    return Map.class.isAssignableFrom(clazz);    
  }

  /** このタイプが配列（リスト）であるか */
  private boolean isArray(Type rawType) {
    if (!(rawType instanceof Class)) throw new AnalyException("Type is not a class");
    Class<?>clazz = (Class<?>)rawType;
    return List.class.isAssignableFrom(clazz);
  }
 
  /**
   * 要素がジェネリックタイプの配列
   * 「Map<String, Integer>[]」など。
   * @param type
   * @return
   */
  JType genericArrayType(GenericArrayType type) {
    return new JArray(generateType(type.getGenericComponentType()));
  }
  
  /**
   * 配列
   * @param clazz
   * @return
   */
  JArray arrayType(Class<?>clazz) {
    return new JArray(generateType(clazz.getComponentType()));
  }
  
  /** 単純クラスのJTypeへの変換表 */
  private static final Map<Class<?>, JType>MAP = new HashMap<Class<?>, JType>() {{
    put(int.class, JInt.PRIMITIVE);
    put(Integer.class, JInt.OBJECT);
    put(String.class, JString.INSTANCE);
    put(long.class, JLong.PRIMITIVE);
    put(Long.class, JLong.OBJECT);
    put(float.class, JFloat.PRIMITIVE);
    put(Float.class, JFloat.OBJECT);
    put(double.class, JDouble.PRIMITIVE);
    put(Double.class, JDouble.OBJECT);
  }};
  
  /** 
   *
   */
  JType simpleClass(Class<?>clazz) { 
    if (clazz.isArray()) throw new AnalyException("internal error");
    JType type = MAP.get(clazz);
    if (type != null) return type;
    
    String className = clazz.getName();
    if (className.startsWith("java.lang.") || className.startsWith("java.util.")) {
      throw new AnalyException(className + " is not supported");
    }
    return generateClass(clazz);
  }
}
