package com.cm55.jaxrsGen.jtype;

public class JBoolean extends JPrimitivable {

  public static final JBoolean PRIMITIVE = new JBoolean(true);
  public static final JBoolean OBJECT = new JBoolean(false);
  
  private JBoolean(boolean primitive) {
    super(primitive);
  }

}
