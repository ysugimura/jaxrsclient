package com.cm55.jaxrsGen.jtype;

public class JMap extends JCompoundType {

  public final JType key;
  public final JType value;

  public JMap(JType key, JType value) {
    this.key = key;
    this.value = value;
  }
  
  @Override
  public String toString() {
    return "(" + key + "," + value + ")";
  }
}
