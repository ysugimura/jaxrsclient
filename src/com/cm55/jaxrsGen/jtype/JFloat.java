package com.cm55.jaxrsGen.jtype;

public class JFloat extends JPrimitivable {
  public static final JFloat PRIMITIVE = new JFloat(true);
  public static final JFloat OBJECT = new JFloat(false);
  
  private JFloat(boolean primitive) {
    super(primitive);
  }
  
  @Override
  public String toString() {
    return "float";
  }
}
