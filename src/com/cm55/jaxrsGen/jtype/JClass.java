package com.cm55.jaxrsGen.jtype;

import java.util.*;
import java.util.stream.*;

public class JClass extends JType {

  /** 元のクラス名称 */
  public String fullClassName;
  
  /** 出力クラス名称。競合時は変更されることがある */
  public String outputClassName;
  
  /** フィールド */
  public final List<JField>fields;
  
  /** クラスコメント */
  public final Optional<String>comment;
  
  public JClass(String fullClassName, String name, List<JField>fields, Optional<String>comment) {
    this.fullClassName = fullClassName;
    this.outputClassName = name;
    this.fields = fields;
    this.comment = comment;
  }
  
  @Override
  public String toString() {
    return "class " + outputClassName + "{\n" +
       fields.stream().map(m->m.toString()).collect(Collectors.joining("\n")) + "\n" +
    "}";
  }

}
