package com.cm55.jaxrsGen.jtype;

public class JInt extends JPrimitivable {
  public static final JInt PRIMITIVE = new JInt(true);
  public static final JInt OBJECT = new JInt(false);
  
  private JInt(boolean primitive) {
    super(primitive);
  }
  
  @Override
  public String toString() {
    return "int";
  }
}
