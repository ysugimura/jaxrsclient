package com.cm55.jaxrsGen.jtype;

public class JArray extends JCompoundType {
  public final JType element;
  
  public  JArray(JType element) {
    this.element = element;
  }
  
  @Override
  public String toString() {
    return "[" + element + "]";
  }
}
