/**
 * Javaの型のうち、JSONでサポート可能な型を表現するもの。
 * <p>
 * 明らかにJSONでサポート可能な「型」よりもJavaの型の方がより広範囲になるのだが、
 * JSONとJavaオブジェクトとの相互変換、あるいは他の言語との相互変換のためには、
 * Javaでの型を制限する必要がある。
 * </p>
 * <p>
 * ここでは、Javaとして許される型を定義する。それらの許される型のみでJAX-RS上の
 * 記述を行い、その許される型のみで型情報を記述することで、Java以外の言語との
 * インターフェースを記述する可能性を望むことにする。
 * </p>
 * <p>
 * 型の構成は次のようになる。
 * </p>
 * <pre>
 * JType
 * +- JCompoundType
 * |  +- JArray
 * |  +- JMap
 * +- JPrimitivable
 * |  +- JInt
 * |  +- JLong
 * |  +- JFloat
 * |  +- JDouble
 * |  +- JBoolean
 * +- JClass
 * +- JString
 * </pre>
 * <pre>
 * JField
 * </pre>
 * 
 */
package com.cm55.jaxrsGen.jtype;