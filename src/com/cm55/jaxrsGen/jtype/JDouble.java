package com.cm55.jaxrsGen.jtype;

public class JDouble extends JPrimitivable {
  public static final JDouble PRIMITIVE = new JDouble(true);
  public static final JDouble OBJECT = new JDouble(true);
  
  private JDouble(boolean primitive) {
    super(primitive);
  }
  
  @Override
  public String toString() {
    return "double";
  }
}
