package com.cm55.jaxrsGen.jtype;

public class JParameter {

  public final String name;
  public final JType type;
  
  public JParameter(String name, JType type) {
    this.name = name;
    this.type = type;
  }
  
  @Override
  public String toString() {
    return type + " " + name;
  }

}
