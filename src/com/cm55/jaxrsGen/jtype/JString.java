package com.cm55.jaxrsGen.jtype;

public class JString extends JType {
  
  public static final JString INSTANCE = new JString();

  @Override
  public String toString() {
    return "string";
  }
}
