/**
 * JAX-RSジェネレータ
 * <p>
 * JAX-RSの定義より、各言語用のクライアントコードを作成する
 * </p>
 * <h2>{@link ClassCollector}</h2>
 * <p>
 * .classファイルからJAX-RSのウェブリソースを検出し、それらが参照するクラス
 * （返り値及び引数）も検出する。ただし、java.lang, java.utilパッケージは除外する。
 * </p>
 * <p>
 * ※あくまでもコンパイル後の.classから取得することに注意。ソースコード.java
 * から取得することはできない。
 * </p>
 * <p>
 * 次のように、対象とするパッケージを指定する。あくまでも、対象パッケージのみ
 * であり、そのサブパッケージは含まれない。
 * </p>
 * <code>
 * ClassCollector c = 
 *   new ClassCollector().append("com.cm55.somePackage");
 * </code>
 * <p>
 * すべてのパッケージの処理が終わったら、getCollection()メソッドによって
 * {@link ClassCollection}を取得する。
 * </p>
 * 
 * <h2>{@link JJClientWriter}</h2>
 * <p>
 * 得られた定義から各言語用のソースコードを出力するが、その一つが、
 * {@link JJClientWriter}である。これは、'com.cm55.jjClient'ライブラリ
 * 用のソースコードを出力するもの。
 * </p>
 * 
 * <h2>メソッドパラメータ名称について</h2>
 * <p>
 * 本ライブラリはコンパイル後の.classファイルからその定義を取得するのだが、
 * 通常、Javaコンパイラはメソッドのパラメータの名称を保持しない。
 * arg0, atrg1, arg2などという名称になってしまう。つまり、
 * </p>
 * <code>
 * void sample(int foo, String bar)
 * </code>
 * は
 * <code>
 * void sample(int arg0, String bar)
 * </code>
 * になってしまう。これを避けるにはコンパイラに-parameterフラグをつけるか、
 * Eclipse環境の場合は、プロジェクトのオプション
 * 「Store information about method parameters (usable via reflection)」
 * をONにする。
 */
package com.cm55.jaxrsGen;
import com.cm55.jaxrsGen.jjClient.*;
