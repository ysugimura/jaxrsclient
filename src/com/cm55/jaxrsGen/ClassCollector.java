package com.cm55.jaxrsGen;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.rs.*;
import com.cm55.jaxrsGen.util.*;

/**
 * 変換対象クラスを収集する
 * @author ysugimura
 */
public class ClassCollector {
    
  // 全ウェブリソースのリスト
  List<RsResource>webResources = new ArrayList<>();

  // 参照クラスプール
  JTypeGenerator jgen = new JTypeGenerator();
  
  // ビルド済
  boolean built = false;

  public ClassCollector append(String pkg) throws Exception {
    notBuild();
    ListClasses.listClasses(pkg).stream().forEach(this::append);
    return this;
  }
  
  void append(Class<?>clazz) {
    notBuild();
    
    // @Pathアノテーションのあるクラスはウェブリソースとする。
    // @PermitAll, @RolesAllowed等はjax-rs用ではないため無視する。
    if (clazz.getAnnotation(javax.ws.rs.Path.class) != null) {        
      webResources.add(new RsResource(jgen, clazz));        
    }    
  }

  /** .classファイルからクラスをロードする */
  Class<?>loadClass(Path classPath, Path f) {
    int dirLength = classPath.toString().length() + 1;
    
    // ファイル名称からクラス名称を取得
    String fileName = f.toString();
    String className = 
      fileName.substring(dirLength, fileName.length() - ".class".length())
      .replace(File.separatorChar,  '.');

    // クラスをロード
    try {
      return Class.forName(className);
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new RuntimeException(ex);
    }    
  }

  /** 
   * クラスコレクションを作成する
   * クラス名競合を解消する。
   * Responseクラスを生成クラスから除去する
   * @return
   */
  public ClassCollection getCollection() {
    notBuild();
    built = true;
    
    // データクラス一覧を取得
    List<JClass>dataClasses = jgen.jclasses().collect(Collectors.toList());
    
    // javax.ws.rs.core.Responseクラスは生成しない。除去する
    dataClasses.removeIf(dc->dc.fullClassName.equals(RsConsts.RESPONSE));
    
    // 重複クラス名を変更
    ClassRenamer renamer = new ClassRenamer();
    webResources.forEach(wr->renamer.renameRsResource(wr));
    dataClasses.forEach(dc->renamer.renameJClass(dc));

    /*
    dataClasses.forEach(dc->System.out.println(dc.fullClassName));
    webResources.forEach(wr->System.out.println(wr.fullClassName));
    */
    
    // コレクションを返す
    return new ClassCollection(webResources, dataClasses);
  }
  
  class ClassRenamer {
    Set<String>names = new HashSet<>();
    
    Optional<String>rename(String className) {
      if (!names.contains(className)) {
        names.add(className);
        return Optional.empty();
      }
      for (int n = 2; ; n++ ) {
        if (!names.contains(className + n)) {
          names.add(className + n);
          return Optional.of(className + n);
        }
      }
      // not reached
    }
    
    void renameRsResource(RsResource r) {
      rename(r.outputClassName).ifPresent(n->
        r.outputClassName = n);
    }
    
    void renameJClass(JClass c) {
      rename(c.outputClassName).ifPresent(n->
        c.outputClassName = n);
    }
  }
  
  private void notBuild() {
    if (built) throw new IllegalStateException("already built");
  }
}
