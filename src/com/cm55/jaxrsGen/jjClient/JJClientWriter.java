package com.cm55.jaxrsGen.jjClient;

import java.io.*;
import java.nio.file.*;

import com.cm55.jaxrsGen.*;
import com.cm55.jaxrsGen.java.*;
import com.cm55.jaxrsGen.util.*;

import static com.cm55.jaxrsGen.util.Rethrow.*;

/**
 * 'com.cm55:jjClient'ライブラリ用のソースコードを作成する。
 * @author ysugimura
 */
public class JJClientWriter extends SourceWriter {

  /**
   * {@link ClassCollector}の定義を使い、ウェブリソースクラスと参照クラス
   * のソースコードを出力する。
   * @param collector {@link ClassCollector}
   * @param topFolder ソース出力フォルダトップパス
   * @param pkgName パッケージ名称。
   *  "foo.bar"の場合はトップパス+"foo/bar"位置に出力される。
   */
  public void generate(ClassCollection collector, Path topFolder, String pkgName) throws IOException {    
    
    // 出力フォルダ
    Path outputFolder = topFolder.resolve(pkgName.replace('.',  '/'));
    DeleteAll.deleteAll(outputFolder);
    Files.createDirectories(outputFolder);
    
    // ウェブリソースの作成
    JJClientWebResourceWriter wrgen = new JJClientWebResourceWriter();      
    collector.webResources().forEach(rethrowC(wr-> {    
      writeString(
        outputFolder.resolve(wr.outputClassName + ".java"), 
        wrgen.generate(wr, pkgName));
    }));    

    // データクラスの作成
    JClassJavaWriter dcgen = new JClassJavaWriter();
    collector.dataClasses().forEach(rethrowC(dc-> {
      writeString(
        outputFolder.resolve(dc.outputClassName + ".java"),
        dcgen.generate(dc, pkgName));
    }));
  }
}
