package com.cm55.jaxrsGen.jjClient;

import java.util.*;

import com.cm55.jaxrsGen.java.*;
import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.rs.*;
import com.cm55.jaxrsGen.util.*;

public abstract class MethodCreator {


  abstract void generate(Stringize e, RsMethod method);

  /**
   * 同期POST/PUTメソッドを作成する
   */
  static MethodCreator POST_PUT_SYNC = new MethodCreator() {
    public void generate(Stringize s, RsMethod method) {
      syncHeader(s, method);

      // メソッド本体
      s.indent();
      try {
        // 呼び出しURLを作成
        constructParam(s, method);

        // コールコード
        s.println("return _jj.%s(_param, %s, %s);", 
          method.httpMethod.lower,
          method.webNoParam.get().name, 
          getTypeDefinition(method.returnType)
        );

      } finally {
        s.unindent();
      }
      s.println("}");
    }
  };
  
  /**
   * 非同期POST/PUTメソッドを作成する
   */
  static MethodCreator POST_PUT_ASYNC = new MethodCreator() {
    public void generate(Stringize s, RsMethod method) {
      asyncHeader(s, method);

      // メソッド本体
      s.indent();
      try {
        // 呼び出しURLを作成
        constructParam(s, method);

        // コールコード
        s.println("_jj.%s(_param, %s, %s, _callback);", 
          method.httpMethod.lower,
          method.webNoParam.get().name, 
          getTypeDefinition(method.returnType),
          getType(method.returnType)
        );

      } finally {
        s.unindent();
      }
      s.println("}");
    }
  };
  
  /**
   * 同期GET/DELETEメソッドを作成する
   */
  static MethodCreator GET_DELETE_SYNC = new MethodCreator() {
    public void generate(Stringize s, RsMethod method) {
      syncHeader(s, method);

      // メソッド本体
      s.indent();
      try {
        // 呼び出しURLを作成
        constructParam(s, method);

        // コールコード
        s.println("return _jj.%s(_param, %s);", 
            method.httpMethod.lower, 
            getTypeDefinition(method.returnType));

      } finally {
        s.unindent();
      }
      s.println("}");
    }
  };

  
  /**
   * 非同期GET/DELETEメソッドを作成する
   * 
   * @param s
   * @param method
   */
  static MethodCreator GET_DELETE_ASYNC = new MethodCreator() {
    void generate(Stringize s, RsMethod method) {
      asyncHeader(s, method);

      // メソッド本体
      s.indent();
      try {
        // 呼び出しURLを作成
        constructParam(s, method);

        // コールコード
        s.println("_jj.%s(_param, %s, _callback);", 
            method.httpMethod.lower,
            getTypeDefinition(method.returnType));
      } finally {
        s.unindent();
      }
      s.println("}");
    }
  };
  
  /**
   * 同期用メソッドヘッダを作成する
   * <code>
   * public 返り値 メソッド名称(パラメータリスト) throws JJException {
   * </code>
   * @param s
   * @param method
   */
  void syncHeader(Stringize s, RsMethod method) {
    // メソッド名称
    s.newline();
    s.comment(method.comment);
    s.println("public %s %s(", getType(method.returnType), method.methodName);

    // メソッドパラメータ、コールバック
    s.indent();
    try {
      parameterList(s, method, false);
    } finally {
      s.unindent();
    }
    s.println(") throws JJException {");    
  }
  
  /**
   * 非同期用メソッドヘッダを作成する
   * <code>
   * public void メソッド名称(パラメータリスト, JJCallback _callback) {
   * </code>
   * @param s
   * @param method
   */
  void asyncHeader(Stringize s, RsMethod method) {

    // メソッド名称
    s.newline();   
    s.comment(method.comment);
    s.println("public void %s(", method.methodName);

    // メソッドパラメータ
    s.indent();
    try {
      parameterList(s, method, true);

    } finally {
      s.unindent();
    }
    s.println(") {");    
  }
  
  /** ウェブメソッドの正規パラメータ出力 */
  void parameterList(Stringize s, RsMethod method, boolean callback) {
    String[]comma = { "" };
    method.webParams().forEach(p-> {   
      s.comment(p.comment);
      s.println(comma[0] + "%s %s", 
        p.getParamTypeString(JTypeStringizerJava.INSTANCE), 
        p.name
      );      
      comma[0] = ", ";
    });
    if (callback) {
      s.println(comma[0] + "JJCallback<%s> _callback", getType(method.returnType));
    }    
  }

  /**
   * 呼び出しパラメータを作成 
   * @param s
   * @param method
   */
  void constructParam(Stringize s, RsMethod method) {
    s.println("JJParam _param = new JJParam(\"%s\");", method.webResource.resourcePath);

    // メソッドパスの指定のあるとき{id}等のパスを置換
    method.methodPath.ifPresent(methodPath -> {

      // このメソッドのパスを追加
      s.println("_param.methodPath(\"%s\");", method.methodPath.get());

      // パスパラメータ置換。urlencodeはライブラリ側でやるので気にしない
      method.webPathParams().forEach(p -> {
        s.println("_param.replacePath(\"%s\", %s);", p.pathParam, p.name);
      });
    });

    // クエリパラメータマップ
    method.webQueryParams().forEach(p->
      s.println("_param.addQuery(\"" + p.queryParam + "\", " + p.name + ");")
    );
  }
  
  /**
   * 型を取得する
   * @param type
   * @return
   */
  String getType(JType type) {
    if (type instanceof JClass && ((JClass)type).fullClassName.equals(RsConsts.RESPONSE))
      return "Void";
    else
      return JTypeStringizerJava.INSTANCE.stringize(type);
  }
  
  /**
   * 型の定義を取得する
   * @param type
   * @return
   */
  String getTypeDefinition(JType type) {
    
    if (type instanceof JClass && ((JClass)type).fullClassName.equals(RsConsts.RESPONSE)) {
      return "Void.class";
    }
    
    String s = JTypeStringizerJava.INSTANCE.stringize(type);
    
    if (type instanceof JCompoundType) {
      return "new GenericType<" + s + ">() {}";
    } else {
      return s + ".class";
    }
  }
  
  
  private static Map<HttpMethod, MethodCreator>SYNC_MAP = new HashMap<HttpMethod, MethodCreator>() {{
    put(HttpMethod.GET, GET_DELETE_SYNC);
    put(HttpMethod.DELETE, GET_DELETE_SYNC);
    put(HttpMethod.POST, POST_PUT_SYNC);
    put(HttpMethod.PUT, POST_PUT_SYNC);
  }};
  
  private static Map<HttpMethod, MethodCreator>ASYNC_MAP = new HashMap<HttpMethod, MethodCreator>() {{
    put(HttpMethod.GET, GET_DELETE_ASYNC);
    put(HttpMethod.DELETE, GET_DELETE_ASYNC);
    put(HttpMethod.POST, POST_PUT_ASYNC);
    put(HttpMethod.PUT, POST_PUT_ASYNC);
  }};
  
  public static void doGenerate(Stringize s, RsMethod method) {
    
    SYNC_MAP.get(method.httpMethod).generate(s, method);
    ASYNC_MAP.get(method.httpMethod).generate(s, method);
  }  
}
