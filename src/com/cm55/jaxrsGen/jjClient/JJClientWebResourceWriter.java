package com.cm55.jaxrsGen.jjClient;

import com.cm55.jaxrsGen.java.*;
import com.cm55.jaxrsGen.rs.*;
import com.cm55.jaxrsGen.util.*;

/**
 * JJClient用のウェブリソースクライアントクラスジェネレータ
 * @author ysugimura
 */
public class JJClientWebResourceWriter implements WebResourceWriter {

  /**
   * 指定された{@link RsResource}のクライアントクラスを作成する
   */
  @Override
  public String generate(RsResource wr, String pkgName) {
    
    Stringize s = new Stringize(JavaCommenter.INSTANCE);

    // 必要なimport
    s.println("package " + pkgName + ";");
    s.println("import com.cm55.jjClient.*;"); // for WJJClient
    s.println("import java.util.*;");
    s.println("import javax.ws.rs.core.*;");
    //s.println("import com.fasterxml.jackson.core.type.TypeReference;");
    s.newline();

    // クラスの作成
    s.comment(wr.comment);
    s.println("public class " + wr.outputClassName + " {");
    s.indent();
    try {
      /* コンストラクタにJJClientを指定し、インスタンスに保持する
       * public final JJClient _jj;
       * public CLASSNAME(JJClient _jj) {
       *   this._jj = _jj;
       * }
       */
      s.println("public final JJClient _jj;");
      s.newline();
      s.println("public " + wr.outputClassName + "(JJClient _jj) {");
      s.indent();
      try {
        s.println("this._jj = _jj;");
      } finally {
        s.unindent();
        s.println("}");
      }

      /*
       * 各メソッドの出力。各メソッドについて同期・非同期版の両方を作成
       */
      wr.webMethods().forEach(m-> {
        MethodCreator.doGenerate(s, m);
      });
      
    } finally {
      // クラスの終了
      s.unindent();
      s.println("}");
    }
    return s.toString();
  }

  
}
