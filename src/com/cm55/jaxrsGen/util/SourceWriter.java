package com.cm55.jaxrsGen.util;

import java.io.*;
import java.nio.file.*;

public class SourceWriter {

  public String encoding = "UTF-8";
  
  public SourceWriter() {    
  }
  
  protected void writeString(Path path, String s) throws IOException {
    byte[]bytes = s.getBytes(encoding);
    Files.write(path, bytes);
  }
}
