package com.cm55.jaxrsGen.util;

import static com.cm55.jaxrsGen.util.Rethrow.*;

import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.jar.*;
import java.util.stream.*;

public class ListClasses {

  /**
   * 指定したパッケージ直下のすべてのクラスを取得する
   * @param pkgName "com.cm55"等
   * @return クラスリスト
   * @throws Exception エラー
   */
  public static Set<Class<?>> listClasses(String pkgName) throws Exception { 

    // 現在のスレッドのローダをクラスローダとする
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    
    // "com/cm55"等のパス
    String pkgPath = pkgName.replace('.', '/');
    
    URL pkgUrl = classLoader.getResource(pkgPath);
    String protocol = pkgUrl.getProtocol();
    
    Stream<String>classStream;    
    
    if (protocol.equals("file")) {
      // フォルダ
      classStream =  Files.list(Paths.get(pkgUrl.toURI()))
        .map(f->f.getFileName().toString()) // ファイル名部分のみ取得
        .filter(n->n.endsWith(".class")) // .classで終始
        .map(n->pkgName + "." + n.substring(0, n.length() - ".class".length()));
      
    } else if (protocol.equals("jar")){    
      // jarファイル内
      try (JarFile jarFile = ((JarURLConnection)pkgUrl.openConnection()).getJarFile()) {
        classStream = Collections.list(jarFile.entries()).stream()
          .map(e->ListClasses.getClassName(e, pkgName, pkgPath))
          .filter(Optional::isPresent)
          .map(n->n.get());       
      }
    } else {
      throw new RuntimeException("unsuppoted");
    }
    
    return classStream    
      .filter(n->n.indexOf('$') < 0)
      .map(rethrowF(n->Class.forName(n))) // クラスを取得
      .collect(Collectors.toSet());    
  }
  
  /**
   * クラス名称を取得する
   * @param e jarエントリ
   * @param pkgName パッケージ名称
   * @return 利用可能なクラス名称
   */
  static Optional<String>getClassName(JarEntry e, String pkgName, String pkgPath) {
    
    if (!e.toString().startsWith(pkgPath + "/")) return Optional.empty();
    String name = e.toString().substring(pkgName.length() + 1);    
    
    if (!name.endsWith(".class") || 
        name.indexOf('/') >= 0 ||
        name.indexOf('$') >= 0) return Optional.empty();
    
    return Optional.of(pkgName + "." + name.substring(0, name.length() - ".class".length()));
  }
}
