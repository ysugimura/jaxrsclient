package com.cm55.jaxrsGen.util;



import java.util.function.*;

/**
 * 以下を参照のこと。
 * 
 * @see <a href="https://www.gwtcenter.com/how-to-use-lambdaexceptionutil">
 * https://www.gwtcenter.com/how-to-use-lambdaexceptionutil</a>
 * @author ysugimura
 */
public final class Rethrow {

  @FunctionalInterface
  public interface Consumer_WithExceptions<T, E extends Exception> {
    void accept(T t) throws E;
  }

  @FunctionalInterface
  public interface BiConsumer_WithExceptions<T, U, E extends Exception> {
    void accept(T t, U u) throws E;
  }

  @FunctionalInterface
  public interface Function_WithExceptions<T, R, E extends Exception> {
    R apply(T t) throws E;
  }

  @FunctionalInterface
  public interface Supplier_WithExceptions<T, E extends Exception> {
    T get() throws E;
  }

  @FunctionalInterface
  public interface Runnable_WithExceptions<E extends Exception> {
    void run() throws E;
  }

  public static <T, E extends Exception> Consumer<T> rethrowC(Consumer_WithExceptions<T, E> consumer) throws E {
    return uncheckC(consumer);
  }

  public static <T, E extends Exception> Consumer<T> uncheckC(Consumer_WithExceptions<T, E> consumer) {
    return t -> {
      try {
        consumer.accept(t);
      } catch (Exception exception) {
        throwAsUnchecked(exception);
      }
    };
  }

  public static <T, U, E extends Exception> BiConsumer<T, U> rethrowB(BiConsumer_WithExceptions<T, U, E> biConsumer) throws E {
    return uncheckB(biConsumer);
  }

  public static <T, U, E extends Exception> BiConsumer<T, U> uncheckB(BiConsumer_WithExceptions<T, U, E> biConsumer) {
    return (t, u) -> {
      try {
        biConsumer.accept(t, u);
      } catch (Exception exception) {
        throwAsUnchecked(exception);
      }
    };
  }

  public static <T, R, E extends Exception> Function<T, R> rethrowF(Function_WithExceptions<T, R, E> function) throws E {
    return uncheckF(function);
  }

  public static <T, R, E extends Exception> Function<T, R> uncheckF(Function_WithExceptions<T, R, E> function)  {
    return t -> {
      try {
        return function.apply(t);
      } catch (Exception exception) {
        throwAsUnchecked(exception);
        return null;
      }
    };
  }

  public static <T, E extends Exception> Supplier<T> rethrowS(Supplier_WithExceptions<T, E> supplier) throws E {
    return uncheckS(supplier);
  }

  public static <T, E extends Exception> Supplier<T> uncheckS(Supplier_WithExceptions<T, E> supplier)  {
    return () -> {
      try {
        return supplier.get();
      } catch (Exception exception) {
        throwAsUnchecked(exception);
        return null;
      }
    };
  }

  public static <E extends Exception> Runnable rethrowR(Runnable_WithExceptions<E> runnable) throws E {
    return uncheckR(runnable);
  }

  public static <E extends Exception> Runnable uncheckR(Runnable_WithExceptions<E> runnable)  {
    return () -> {
      try {
        runnable.run();
      } catch (Exception exception) {
        throwAsUnchecked(exception);
      }
    };
  }

  /**
   * 以下の「検査例外を非検査例外に」を参照のこと。
   * https://www.gwtcenter.com/how-to-use-lambdaexceptionutil
   * @param <E>
   * @param exception
   * @throws E
   */
  @SuppressWarnings("unchecked")
  public static <E extends Throwable> void throwAsUnchecked(Exception exception) throws E {
    throw (E) exception;
  }
}