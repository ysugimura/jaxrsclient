package com.cm55.jaxrsGen.util;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class DeleteAll {

  public static void deleteAll(Path doc) throws IOException {
    if (Files.exists(doc)) 
      Files.walk(doc).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
  }

}
