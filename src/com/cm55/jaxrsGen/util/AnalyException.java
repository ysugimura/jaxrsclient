package com.cm55.jaxrsGen.util;

public class AnalyException extends RuntimeException {

  public AnalyException(String msg) {
    super(msg);
  }

}
