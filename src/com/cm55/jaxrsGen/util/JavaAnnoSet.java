package com.cm55.jaxrsGen.util;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * もともとのJavaのアノテーションを保持する集合。
 * 
 * @author ysugimura
 */
public class JavaAnnoSet {

  private Map<Class<?>, Annotation>map = new HashMap<>();
  
  @SuppressWarnings("unchecked")
  public JavaAnnoSet(Annotation[]annotations) {
    map = Arrays.stream(annotations)
        .collect(Collectors.toMap(a->(Class<Annotation>)a.annotationType(),  a->a));
  }
  
  public Stream<String>names() {
    return map.values().stream().map(a->a.annotationType().getName());    
  }
  
  /**
   * コメントがあれば取得する
   * Commentアノテーションのパッケージは問わない。
   * String value()というメソッドが存在すること。
   */
  public Optional<String>getComment() {
    Method[]method = new Method[1];    
    return map.keySet().stream()
      .filter(c-> {
        // アノテーション型が「～.Comment」であること
        if (!c.getName().endsWith(".Comment")) 
          return false;
        
        // Stringを返すvalue()メソッドがあること
        try {
          method[0] = c.getMethod("value");
          if (!method[0].getReturnType().equals(String.class))
            return false;            
          return true;
        } catch (Exception ex) {
          return false;
        }
      })
      .findAny()
      .map(clazz-> {
        Annotation anno = map.remove(clazz);
        try {
          return (String)method[0].invoke(anno);    
        } catch (Exception ex) {
          throw new RuntimeException(ex);
        }
      });
  }

  /** 
   * 指定クラスされたクラスのいずれかのアノテーションオブジェクトを除去し、
   * リストとして返す。
   */
  @SuppressWarnings("unchecked")
  public <T extends Annotation>List<T> remove(Class<? extends Annotation>[]classes) {    
    return Arrays.stream(classes).map(c->(Optional<T>)removeOptional(c))
      .filter(o->o.isPresent()).map(o->o.get()).collect(Collectors.toList());
  }
  
  /** 
   * 指定クラスされたクラスのいずれかのアノテーションオブジェクトを除去して返す。
   * 存在しなければ例外 */
  @SuppressWarnings("unchecked")
  public <T extends Annotation, S>S 
    remove(Class<? extends Annotation>[]classes, Function<T, S>[]functions) {        
    Optional<Integer>opt = 
      IntStream.range(0,  classes.length)    
      .filter(i->map.containsKey(classes[i])).mapToObj(i->i).findAny();
    
    return opt.map(i-> {      
      return (S)functions[i].apply((T)map.remove(classes[i]));
    }).orElseThrow(()-> new RuntimeException("No annotation of " + 
          Arrays.stream(classes).map(c->c.getSimpleName()).collect(Collectors.joining(","))
    ));    
  }
  
  /** 指定されたクラスのアノテーションを除去して返す。存在しなければ例外 */
  public <T extends Annotation>T remove(Class<T>clazz) {    
    return removeOptional(clazz)
     .orElseThrow(()->new RuntimeException("No annotation of " + clazz.getSimpleName()));
  }
  
  /** 指定されたクラスのアノテーションを除去し、さらにFunctionで変換して返す。存在しなければ例外 */
  public <T extends Annotation, S>S remove(Class<T>clazz, Function<T, S>mapper) {    
    return removeOptional(clazz).map(mapper)
     .orElseThrow(()->new RuntimeException("No annotation of " + clazz.getSimpleName()));
  }
  

  /** 指定アノテーションがあればそれを除去して返す。なければ空を返す。 */
  @SuppressWarnings("unchecked")
  public <T extends Annotation>Optional<T> removeOptional(Class<T>clazz) {
    return Optional.ofNullable((T)map.remove(clazz));
  }
  
  /** 
   * 指定アノテーションがあればそれを除去し、さらにFunctionで変換して返す。 
   * なければ空を返す。
   */
  @SuppressWarnings("unchecked")
  public <T extends Annotation, S>Optional<S> removeOptional(Class<T>clazz, Function<T, S>mapper) {
    return Optional.ofNullable((T)map.remove(clazz)).map(mapper);
  }
  
  /** 残っているすべてのアノテーションをストリームとして取得する */
  public Stream<Annotation>stream() {
    return map.values().stream();
  }
}
