package com.cm55.jaxrsGen.util;

public class Commenter {

  public final String open;
  public final String middle;
  public final String close;
  public Commenter(String open, String middle, String close) {
    this.open = open;
    this.middle = middle;
    this.close = close;
  }

}
