package com.cm55.jaxrsGen.util;

import com.cm55.jaxrsGen.rs.*;

public interface WebResourceWriter {

  public String generate(RsResource wr, String pkgName);

}
