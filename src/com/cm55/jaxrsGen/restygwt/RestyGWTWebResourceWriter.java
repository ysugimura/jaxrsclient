package com.cm55.jaxrsGen.restygwt;

import com.cm55.jaxrsGen.java.*;
import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.rs.*;
import com.cm55.jaxrsGen.rs.RsParameter.*;
import com.cm55.jaxrsGen.util.*;

/**
 * RestyGWT用コード生成
 * @author ysugimura
 */
public class RestyGWTWebResourceWriter implements WebResourceWriter {

  /**
   * {@link RsResource}クラスについて作成する
   */
  @Override
  public String generate(RsResource wr, String pkgName) {
    
    Stringize s = new Stringize(JavaCommenter.INSTANCE);
    
    // package name, imports
    s.println("package %s;", pkgName)
      .println("import javax.ws.rs.*;")
      .println("import org.fusesource.restygwt.client.*;");
        
    // start class
    s.println("");
    s.comment(wr.comment);
    s.println("@Path(\"%s\")", wr.resourcePath); 
    s.println("public interface %s extends RestService {",  wr.outputClassName);
    
    // 各メソッドを出力
    s.indent();
    try {
      wr.webMethods().forEach(m->generateMethod(s, m));
    } finally {
      s.unindent();
    }
    
    // end of class
    s.println("}");
    
    return s.toString();
  }
  
  /** 
   * {@link RsMethod}メソッドを出力する 
   * @param method
   * @return
   */
  void generateMethod(Stringize s, RsMethod method) {
    
    // 行を開ける
    s.newline();
    
    // コメント
    s.comment(method.comment);
    
    // リクエストメソッドを指定、GET、POST等
    s.println("@" + method.httpMethod);
    
    // @Pathがあれば指定
    method.methodPath.ifPresent(p-> s.println("@Path(\"%s\")", p));
    
    // @Consumes/@Produces
    method.consumes.ifPresent(c->s.println(c.toString()));
    method.produces.ifPresent(p->s.println(p.toString()));
    
    // メソッドパラメータリストを作成
    s.println("public void %s (", method.methodName);
    s.indent();
    try {
      String[]continued = { "" };
      method.webParams().forEach(p-> {
        s.comment(p.comment);
        s.println(continued[0] + getParameter(p));
        continued[0] = ",";
      });
      String returnType = 
          method.getReturnTypeString(JTypeStringizerJava.INSTANCE);
      if (method.returnType instanceof JClass &&
        ((JClass)method.returnType).fullClassName.equals(RsConsts.RESPONSE)) {
        returnType = "Void";
      }      
      s.println(continued[0] + 
        "MethodCallback<%s> callback",
        returnType
      );
        
    } finally {
      s.unindent();
    }
    s.println(");");
  }

  /** 一つのパラメータを文字列化する */
  String getParameter(RsParameter p) {
    if (p instanceof RsPathParam) {
      RsPathParam wp = (RsPathParam)p;
      return  "@PathParam(\"" + wp.pathParam + "\") " +
        JTypeStringizerJava.INSTANCE.stringize(wp.type) + " " + wp.name;
    }
    if (p instanceof RsQueryParam) {
      RsQueryParam wp = (RsQueryParam)p;
      return "@QueryParam(\"" + wp.queryParam + "\") " +
        JTypeStringizerJava.INSTANCE.stringize(wp.type) + " " + wp.name;
    }
    if (p instanceof RsNoParam) {
      RsNoParam wp = (RsNoParam)p;
      return JTypeStringizerJava.INSTANCE.stringize(wp.type) + " " + wp.name;
    }
    throw new RuntimeException("NOT SUPPORTED");
  }

}
