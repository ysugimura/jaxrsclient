package com.cm55.jaxrsGen.restygwt;

import java.io.*;
import java.nio.file.*;

import com.cm55.jaxrsGen.*;
import com.cm55.jaxrsGen.java.*;
import com.cm55.jaxrsGen.util.*;

import static com.cm55.jaxrsGen.util.Rethrow.*;

/**
 * {@link ClassCollector}の定義を使い、RestyGWT用のウェブリソースクラスと参照クラス
 * のソースコードを出力する。
 */
public class RestyGWTWriter extends SourceWriter  {

  /**
   * {@link ClassCollector}の定義から必要なクラスのソースを出力する
   * @param collector {@link ClassCollector}
   * @param topFolder ソース出力フォルダトップ
   * @param pkgName ベースパッケージ名称。ただし、
   * "foo.bar"の場合はoutput + "foo/bar/client"に出力され、
   * "foo.bar.client"というパッケージ名称になる。
   * @param gwtXml gwt.xmlのベース名称。"/foo/bar/${gwtXml}.gwt.xml"という
   * ファイル名称になる。
   * @throws IOException 書き込みエラー
   */
  public void generate(ClassCollection collector, 
      Path topFolder, String pkgName, String gwtXml) throws IOException {

    // 出力対象フォルダ
    Path outputFolder = topFolder.resolve(pkgName.replace('.',  '/'));
    DeleteAll.deleteAll(outputFolder);
    Files.createDirectories(outputFolder);

    // gwt.xmlを出力する
    Files.write(
      outputFolder.resolve(gwtXml + ".gwt.xml"),
      "<module></module>".getBytes());
    
    // pkgName, topFolderを変更する
    String clientPkgName = pkgName + ".client";
    Path clientOutputFolder = outputFolder.resolve("client");
    Files.createDirectories(clientOutputFolder);
    
    // ウェブリソースを出力する
    RestyGWTWebResourceWriter wrgen = new RestyGWTWebResourceWriter();    
    collector.webResources().forEach(rethrowC(wr-> {    
      writeString(
        clientOutputFolder.resolve(wr.outputClassName + ".java"),
        wrgen.generate(wr, clientPkgName)
      );
    }));    

    // データクラスを出力する
    JClassJavaWriter dcgen = new JClassJavaWriter();
    collector.dataClasses().forEach(rethrowC(dc-> {
      writeString(
        clientOutputFolder.resolve(dc.outputClassName + ".java"),
        dcgen.generate(dc, clientPkgName)
      );
    }));
  }
}
