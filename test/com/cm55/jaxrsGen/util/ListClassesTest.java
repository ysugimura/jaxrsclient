package com.cm55.jaxrsGen.util;

import java.util.*;
import java.util.jar.*;
import java.util.stream.*;

import org.junit.*;

public class ListClassesTest {

  /*
  @Test
  public void test() throws Exception {

    ListClasses.listClasses("org.junit").stream()
      .forEach(c->System.out.println(c.getName()));
    
  }
  */
  
  @Test
  public void test2() throws Exception {
    String pkgName = "nato.base.webServer.devel";
    String pkgPath = "nato/base/webServer/devel";
    // jarファイル内
    Stream<String>classStream;
    try (JarFile jarFile = new JarFile("webServer-1.8.jar")) {
      classStream = Collections.list(jarFile.entries()).stream()
        .map(e->ListClasses.getClassName(e, pkgName, pkgPath))
        .filter(Optional::isPresent)
        .map(n->n.get());       
    }
    classStream.forEach(System.out::println);
  }


}
