package com.cm55.jaxrsGen.jtype;

import java.util.*;

import org.junit.*;

public class JTypeGeneratorTest {

  @Test
  public void test() {
    JClass clazz = new JTypeGenerator().generateClass(Foo.class);
    System.out.println(clazz);
  }
  
  public static class Foo  {
    /*
    public Map<Float, Integer>foo(Bar a, String b) {
      return null;
    }
    
    public int foo2(ArrayList<String>list) {
      return 0;
    }
    */
    public double foo3(Map<String, Integer>[]list) {
      return 0D;
    }
  }

}
