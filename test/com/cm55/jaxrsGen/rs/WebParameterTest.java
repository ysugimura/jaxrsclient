package com.cm55.jaxrsGen.rs;

import java.lang.reflect.*;
import java.util.*;

import javax.ws.rs.*;

import org.junit.*;

import com.cm55.jaxrsGen.java.*;
import com.cm55.jaxrsGen.jtype.*;
import com.cm55.jaxrsGen.rs.RsParameter.*;

public class WebParameterTest {

  @Test
  public void test() throws Exception {
    Method method = WebParameterTest.class.getMethod("sample", 
          String.class, Bar.class, Foo.class);
    
    Arrays.stream(method.getParameters()).forEach(parameter-> {
      RsParameter p = RsParameter.create(new JTypeGenerator(), parameter);
    
      if (p instanceof RsPathParam) {
        RsPathParam wp = (RsPathParam)p;
        System.out.println("@PathParam(\"" + wp.pathParam + "\") " +
          JTypeStringizerJava.INSTANCE.stringize(wp.type) + " " + wp.name);
      }
      if (p instanceof RsQueryParam) {
        RsQueryParam wp = (RsQueryParam)p;
        System.out.println("@QueryParam(\"" + wp.queryParam + "\") " +
          JTypeStringizerJava.INSTANCE.stringize(wp.type) + " " + wp.name);
      }
      if (p instanceof RsNoParam) {
        RsNoParam wp = (RsNoParam)p;
        System.out.println(JTypeStringizerJava.INSTANCE.stringize(wp.type) + " " + wp.name);
      }
    });    
  }

  public void sample(@PathParam("id") String id, @QueryParam("query") Bar bar, Foo foo) {
    
  }
  
  public static class Bar {
    
  }
  public static class Foo {
    
  }
}
