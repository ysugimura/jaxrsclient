package test;
import java.io.*;
import java.nio.file.*;

import com.cm55.jaxrsGen.*;
import com.cm55.jaxrsGen.jjClient.*;
import com.cm55.jaxrsGen.restygwt.*;
import com.cm55.jaxrsGen.util.*;

public class GenTest {

  public static void main(String[]args) throws Exception {
    File dir = ClassPathLocator.getLocation();    
    
    ClassCollector collector = new ClassCollector().append("test.api");
    ClassCollection collection = collector.getCollection();
    
    
    
    new RestyGWTWriter().generate(
        collection,
        Paths.get("testGWT"), 
        "sample",
        "Sample"
    ); 
    
    
    new JJClientWriter().generate(
collection,
        Paths.get("testJJ"), "sample"
    );
    
  }
}
