package test.api;


import javax.ws.rs.*;
import javax.ws.rs.core.*;



@Path("/books")
public class Books {

  

  
  @Comment("指定IDの従業員データを取得する")
  @Path("/{id}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Book getById(
    @Comment("取得する本のID")
    @PathParam("id") int id
  ) {
    //log.info("getById " + id);
    return new Book(123, "小林");
  }
}
