package test.api;

/**
 * サンプル従業員
 * @author ysugimura
 */
@Comment("サンプル従業員")
public class Employee {

  @Comment("従業員ID")
  public int id;
  
  @Comment("従業員名称")
  public String name;
  
  public Employee() {}
  public Employee(int id, String name) {
    this.id = id;
    this.name = name;
  }
  
  @Override
  public String toString() {
    return "id:" + id + ", name:" + name;
  }
  
  public static Employee valueOf(String value) {
    System.out.println("value=" + value);
    return new Employee();
}
}
