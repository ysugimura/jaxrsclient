package test.api;

import java.util.*;
import java.util.stream.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;


@Comment("従業員に関する操作\n二行目\n産業め")
@Path("employees")
public class Employees {

  static final Employee[]EMPLOYEES = {
      new Employee(1, "佐藤"),
      new Employee(2, "鈴木"),
      new Employee(3, "小林"),
      new Employee(4, "山本"),
  };
  
  @Path("/test")
  @POST
  public Response test1(int a) {
    return Response.ok().build();
  }

  @Path("/test")
  @GET
  public Response test() {
    return Response.ok().build();
  }
  
  static final Map<Integer, Employee>MAP = 
      Arrays.stream(EMPLOYEES).collect(Collectors.toMap(a->a.id, a->a));
  
  @Comment("idを指定して単一のEmployeeデータを取得する")
  @Path("/{id}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Employee getById(
      @Comment("指定する従業員ID")
      @PathParam("id") 
      int id,
      @QueryParam("option")
      String option
    ) {
    System.out.println("getById entry");
    Employee e = MAP.get(id);
    System.out.println("getById exit");
    return e;
  }
  
  @Comment("単一のEmployeeデータを更新する")
  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  public Response put(Employee e) {
    throw new RuntimeException();
  }
  
  @Comment("すべての従業員リストを取得する")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Employee[]getAll() {
    System.out.println("getAll entry");
    Employee[]es = EMPLOYEES;
    System.out.println("getAll exit");
    return es;
  }
  
  @Comment("すべての従業員リストを取得する")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Employee[]getAll111(int a ) {
    System.out.println("getAll entry");
    Employee[]es = EMPLOYEES;
    System.out.println("getAll exit");
    return es;
  }
  
  @Comment("指定した従業員データを書き込み、そのデータを返す")
  @POST
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Employee post(
      @Comment("書き込み従業員のID")
      @PathParam("id")
      int id,
      
      @Comment("書き込む従業員データ")
      Employee employee
    ) {
    System.out.println("post " + employee);
    return employee;
  } 
  
  
}