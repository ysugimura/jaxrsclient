package test.api;

import java.lang.annotation.*;

/**
 * テスト用コメントアノテーション
 * クラス、メソッド、パラメータ、フィールドに使用可
 * @author ysugimura
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Comment {
    String value();
}
